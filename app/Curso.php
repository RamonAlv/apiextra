<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use Notifiable;

    protected $fillable = [
        'name', 'description', 'fecha', 'costo', 'entradas', 'status',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function scopeReporte($query){

        return $query->orderBy('id','desc');

    }

    public function users(){
        return $this->belongsToMany(User::class);
    }

    public function usersc(){
        return $this->belongsTo(User::class);
    }
}
