<?php

namespace App\Http\Controllers\API\Curso;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Curso;
use App\User;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curso = Curso::all();
        return response()->json(['data'=>$curso], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addClase(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->cursosc()->create([
            'name' => $request->name,
            'description' => $request->description,
            'fecha' => $request->fecha,
            'costo' => $request->costo,
            'entradas' => $request->entradas
        ]);
        //$user->cursos()->sync($curso->id);//detach:Eliminar relacion, attach:Agregar relacon, sync:no se repitan las relaciones

        return response()->json(['data' => $user->cursosc], 201);
    }

    public function inscribir(Request $request, $id)
    {
        $idc = $request->id;
        $user = User::findOrFail($id);

        $curso = Curso::findOrFail($idc);
        $use = User::findOrFail($curso->user_id);
        if($curso->entradas > 0){
            $user->cursos()->attach($idc);//detach:Eliminar relacion, attach:Agregar relacon, sync:no se repitan las relaciones
            $curso->entradas = ($curso->entradas-1);
            $curso->saveOrFail();
            return response()->json(['creador' => $use->name, 'curso'=> $user->cursos], 201);
        }else{
            $curso->status = FALSE;
            $curso->saveOrFail();
            return response()->json(['creador' => $use->name, 'curso lleno'=> $curso->status], 201);
        }
    }

    public function inscritos($id){
        $curso = Curso::findOrFail($id);
        $user = User::findOrFail($curso->user_id);
        return response()->json(['creador' => $user->name, 'curso' => $curso->name, 'inscritos'=> $curso->users], 201);
    }

    public function cursosinscritos($id){
        $user = User::findOrFail($id);
        return response()->json(['cursos inscritos'=> $user->cursos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = null;
        try{

            $curso = Curso::findOrFail($id);
            $msj = "Respuesta Exitosa, code 200";
            $response = array(
                'data'=>$curso
                // 'response'=>$msj,
                // 'status'=>200
            );
        }catch(Exception $e){
            $response = array(
                'data'=>'',
                'response'=>$e->getMessage(),
                'status'=>500
            );
        }
        return response()->json($response, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Curso::findOrFail($id);
        $curso->name = $request->name;
        $curso->description = $request->description;
        $curso->fecha = $request->fecha;
        $curso->costo = $request->costo;
        $curso->entradas = $request->entradas;

        $curso->saveOrFail();

        return response()->json(['data' => $curso], 200);
    }

    public function updateEntrada(Request $request, $id)
    {
        $curso = Curso::findOrFail($id);
        $curso->entradas = $request->entradas;

        $curso->saveOrFail();

        return response()->json(['data' => $curso], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $idu = $request->id;
        $user = User::findOrFail($idu);
        $user->cursos()->detach($id);//detach:Eliminar relacion, attach:Agregar relacon, sync:no se repitan las relaciones
        return response()->json(['data' => $user->usersc], 201);
    }
}
