<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'API\User\UserController@login');
    Route::resource('users', 'API\User\UserController', ['except' =>['create', 'edit']]);
    Route::post('cursos/crear/{user}', 'API\Curso\CursoController@addClase');
    Route::post('cursos/inscribir/{user}', 'API\Curso\CursoController@inscribir');
    Route::get('usuario/inscritos/{curso}', 'API\Curso\CursoController@inscritos');
    Route::get('cursos/inscritos/{user}', 'API\Curso\CursoController@cursosinscritos');
    Route::resource('cursos', 'API\Curso\CursoController', ['except' =>['create', 'edit']]);
});
